from bokeh.io import curdoc
from bokeh.models.widgets import Tabs
from os.path import dirname, join
import pandas as pd
from hist import hist_tab

data=pd.read_csv("../csv/flights.csv", index_col=0).dropna()
# print(len(data))
hist_tab(data)



# BokehDeprecationWarning: 'WidgetBox' is deprecated and will be removed in Bokeh 3.0, use 'bokeh.models.Column' instead