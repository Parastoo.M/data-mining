import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool
from bokeh.palettes import Pastel1_5
from bokeh.transform import factor_cmap
output_file('3PaThor.html')

data = pd.read_csv('../csv/thor_wwii.csv')
dataGrouped = data.groupby('COUNTRY_FLYING_MISSION')['TOTAL_TONS','TONS_FRAG',
'TONS_IC','TONS_HE'].sum()
# print(dataGrouped)


dataSource = ColumnDataSource(dataGrouped)
countries = dataSource.data['COUNTRY_FLYING_MISSION'].tolist()
p = figure(x_range = countries)


cm = factor_cmap(field_name='COUNTRY_FLYING_MISSION', palette = Pastel1_5, factors = countries)


p.vbar(source = dataSource , x = 'COUNTRY_FLYING_MISSION' , top = 'TOTAL_TONS' , width = 0.8 , 
color =cm)
h = HoverTool()
h.tooltips = [
    ('جمع',
    'جمع مواد منفجره تفکیک های@TONS_HE است و حجم مواد آتش زا @TONS_IC است. و تعداد @TONS_FRAG منفجر شده است.')
]
h.mode = 'vline'
p.add_tools(h)
show(p)
