import pandas as pd
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool
from bokeh.plotting import figure,output_file,show
from bokeh.palettes import Category10
from bokeh.transform import factor_hatch
from bokeh.themes import built_in_themes
from bokeh.io import curdoc
output_file('4Pathor.html')

data = pd.read_csv('../csv/thor_wwii.csv')
data['MSNDATE']= pd.to_datetime(data['MSNDATE'], format=('%m/%d/%Y'))
datagroped = data.groupby(pd.Grouper(key='MSNDATE', freq='M'))['TOTAL_TONS','TONS_FRAG','TONS_IC','TONS_HE'].sum()
dataSource =ColumnDataSource(datagroped)

p = figure(x_axis_type='datetime')
p.line(x = 'MSNDATE' , y = 'TOTAL_TONS',source=dataSource, color='green', line_width=3,legend_label = 'total')
p.legend.click_policy= 'hide'
show(p)

