import pandas as pd
from bokeh.plotting import figure, output_file,show
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool

output_file('2btc.html')

dataBTC = pd.read_csv('../csv/BTC-USD.csv')
dataSourc = ColumnDataSource(dataBTC)

p = figure()
p.circle( source= dataSourc  , x='Volume', y='High', color='red' )
p.title.text="btc_USA"
p.xaxis.axis_label='volume'
p.yaxis.axis_label='high'

h = HoverTool()
h.tooltips=[
    ('time is','@Date'),
    ('low is','@Low')
]
p.add_tools(h)
show(p)