import pandas as pd
from bokeh.plotting import figure,output_file,show
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool

output_file("btc.html")

dataB=pd.read_csv('../csv/BTC-USD.csv')
dataB['Date'] = pd.to_datetime(dataB['Date'], format=('%Y/%m/%d'))

dataSourcB= ColumnDataSource(dataB)

pB=figure()

pB.circle(source = dataSourcB, x= 'Date' , y='High',color='red', size=20)
# pB.circle(source=dataSourcB, x= 'dataB' , y='High',color='skyblue', size=20)
pB.title.text="بیت کوین "
pB.xaxis.axis_label='کمترین'
pB.yaxis.axis_label="بیشترین"

show(pB)